import csv
import numpy as np
from itertools import zip_longest

class dataLogger():
    def __init__(self):
        #self.reader=csv.reader()
        pass
    def saveFile(self,name,timeStamp,gsrData,HeartData,HRData,Stress):
        try:
            with open(str(name + '.csv'), 'w') as self.file:
                self.writer=csv.writer(self.file,lineterminator='\n',delimiter=',',quotechar=' ', quoting=csv.QUOTE_MINIMAL)
                self.writer.writerow(['TIME','GSR','BPM','HR','STRESS'])
                
                print (len(timeStamp))
                print (len(gsrData))
                print (len(HRData))
                print (len(Stress))  
                rows = zip_longest(timeStamp,gsrData,HeartData,HRData,Stress)
                for values in zip_longest(rows):
                    self.writer.writerows(values)
             
        except PermissionError:
            print ("File is open elsewhere")
    def readFile(self):
        pass
    

if __name__=="__main__":
    timeStamp=[1,2,3,4,5,6,7,8,9,10]
    GSR=[11,22,33,44,55,66,77,88,99,1010]
    HR=[12,23,34,45,56,67,78,89,90,100]
    stress=[21,22,232,24,25]

    logger=dataLogger()
    logger.saveFile("givenName",timeStamp,GSR,HR,stress)