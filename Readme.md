The GUI was designed with the help
wxPython distribution. wxPython is a wrapper for the crossplatform GUI API (often referred to as a "toolkit") wxWidgets (which is written
in C++) for the Python programming language. It is one of the alternatives
to Tkinter, which is bundled with Python. It is implemented as a Python extension
module (native code). Other popular alternatives are PyGTK, its
successor PyGObject and PyQt. Like wxWidgets, wxPython is free software.
The GUI developed has a similar interface as below:




**System overview**

The whole measuring system can be sub divided into two smaller system. One
being the measuring and transmitting device and other is receiver which sends the
data to the software for plotting and viewing data.
The transmitter consists of the following parts:

1.Xbee series2

2.Skin conductance measurement sensor

3.Pulse oximeter

4.Battery

5.Different indicator LEDs


Electrodermal activity of skin is captured by GSR electrode. GSR sensor
circuit converts the change in skin resistance to suitable voltage signal. This
voltage signal is then fed into Xbee through one of its analog input pin. Similar
purpose is achieved by the ear clip pulse oximeter. Xbee wirelessly sends this data
to another xbee on the receiving side. A dc battery power supply constantly
provides power to the xbee and other sensors.
The receiving circuit is fairly simple and consists of the following parts:

1.Recieving xbee

2.Suitable usb to serial converter(CP2102)

