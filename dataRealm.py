import time
import serial
import time
from threading import Thread
from xbee import ZigBee
import numpy as np


class dataRealm():
    def __init__(self):
        self.selected_baudrate= self.baud_choice.GetString(self.baud_choice.GetSelection())
        self.selected_com_port= self.com_choice.GetString(self.com_choice.GetSelection())
    
    def getBPM(self):
        self.timeElapsed= int(time.time()-self.START_TIME)      
        #print("####")
        #print(self.timeElapsed)
        if(self.timeElapsed>=2):
            self.bpm=round((self.beatCount/self.timeElapsed)*60)
            #print("#####" ,end='')
            #print(self.bpm)
            self.beatCount=0
            self.HRAvgList[self.HRAvgCounter]=self.bpm
            #print (self.HRAvgList)
            self.HRAvgCounter=self.HRAvgCounter+1
            if(self.HRAvgCounter==len(self.HRAvgList)):
                self.HRAvgCounter=0
            self.START_TIME=time.time()
            self.BPM=round(sum(self.HRAvgList)/len(self.HRAvgList))
            self.textBpm.SetValue(str(self.BPM))
        return self.BPM
    def getGSRData(self):
        pass
    def getHRData(self):
        pass
    def getStressData(self):
        pass
    