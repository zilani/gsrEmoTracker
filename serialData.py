import serial
import time
from threading import Thread
from xbee import ZigBee

import numpy as np

last_received=dict()

def receiving(xbee):
    global last_received
    while True:
        last_received=xbee.wait_read_frame()
        #print (last_received)
        #print (last_received['samples'][0]['adc-1'])
        
        
class serialInit():

    def __init__(self,COMPORT,BAUDRATE):
        self.XbeeSerialPort=serial.Serial()
        self.XbeeSerialPort.baudrate = BAUDRATE
        self.XbeeSerialPort.port = COMPORT
        self.XbeeSerialPort.timeout=20
        
        try:
            self.XbeeSerialPort.open()
            self.xbee=ZigBee(self.XbeeSerialPort)
        except serial.serialutil.SerialException:
            # no serial connection
            self.XbeeSerialPort = None
            raise TypeError("NO PORT DETECTED")
        
        else:
            receiver=Thread(target=receiving, args=(self.xbee,))
            receiver.daemon=True
            receiver.start()
            
    
    def __del__(self):
        if self.XbeeSerialPort is not None:
            self.XbeeSerialPort.close()
    
    def getGsrData(self):
        global last_received
        for i in range(50):
            try:
                return last_received['samples'][0]['adc-0']
            except KeyError:
                #print("NNOO")
                time.sleep(0.01)
                return -1 
        return 0
    
    def getHrData(self):
        global last_received
        for i in range(50):
            try:
                return last_received['samples'][0]['dio-4']
            except KeyError:
                #print("NNOO")
                time.sleep(0.01)
                return -1 
        return 0
        
              
if __name__=="__main__":
    x=serialInit(COMPORT='COM3',BAUDRATE=115200)
    x.getGsrData()
    time.sleep(20)