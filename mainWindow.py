#!/usr/bin/env python

#import argparse

import serialData
import os
import wx
import numpy as np
import serial
import serial.tools.list_ports


import datetime
import matplotlib
from matplotlib.figure import Figure
import time

import dataLogger
"""from Arduino_Monitor import SerialData"""

# The recommended way to use wx with mpl is with the WXAgg backend.
matplotlib.use('WXAgg')

# Those import have to be after setting matplotlib backend.
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigCanvas  # noqa
import matplotlib.pyplot as plt  # noqa


REFRESH_INTERVAL_MS =30
DPI = 20

#HR_REFRESH_INTERVAL_MS=200

class GraphFrame(wx.Frame):
    """The main frame of the application."""

    title = 'CUET EEE:: SKIN CONDUCTANCE ANALYSIS'
    designedHRshape=[260,253,244,300,240,250]
    HRshapeIter=0
    START_TIME=time.time()
    PROGRAM_START_TIME=time.time()
    timeStamp=[]
    beatCount=0
    HRAvgList= [70,70,70,70,70,70,70,70,70,70,70,70,70,70,70]
    HRAvgCounter=0
    BPM=0
    prevHrd=False
    def __init__(self):
        wx.Frame.__init__(self, None, -1, self.title)
        self.Maximize(True)
        self.paused = False
        self.started= False
        self.selected_baudrate=None
        self.selected_com_port=None
        
        
        self.create_menu()
        self.create_status_bar()
        self.create_window()
          

    def create_menu(self):
        self.menu_bar = wx.MenuBar()
        menu = wx.Menu()
        menu.AppendSeparator()
        self.menu_bar.Append(menu, "&File")
        
        take_shot_entry = menu.Append(
            id=-1,
            item="&Take screenshot\tCtrl-S",
            helpString="Save png image of plot"
        )
        self.Bind(wx.EVT_MENU, self.on_plot_save, take_shot_entry)

        exit_entry = menu.Append(
            id=-1,
            item="&Exit\tCtrl-X",
            helpString="Exit"
        )
        self.Bind(wx.EVT_MENU, self.on_exit, exit_entry)
        self.SetMenuBar(self.menu_bar)
    
    def create_window(self):

        self.panel = wx.Panel(self)
        
        self.create_graph_plots()
        self.create_control_elements()
        
        self.canvas = FigCanvas(self.panel, -1, self.figure)
        
        self.vbox = wx.BoxSizer(wx.HORIZONTAL)
        self.vbox.Add(self.canvas, 1, flag=wx.ALL )
        
        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.Add(self.start_button,border=5,flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(15)
        self.hbox1.Add(self.pause_button,border=5,flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(15)
        self.hbox1.Add(self.grid_visibility_check_box,border=5,flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(10)
        self.hbox1.Add(self.xlabels_visibility_check_box,border=5,flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(250)
        self.hbox1.Add(self.BPM_text,border=10,flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.Add(self.textBpm,border=10,flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        self.hbox1.AddSpacer(100)
        self.hbox1.Add(self.save_button,border=5,flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        
        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.gsr_xmin_control_box, border=5, flag=wx.ALL)
        self.hbox2.Add(self.gsr_xmax_control_box, border=5, flag=wx.ALL)
        self.hbox2.AddSpacer(20)
        self.hbox2.Add(self.gsr_ymin_control_box, border=5, flag=wx.ALL)
        self.hbox2.Add(self.gsr_ymax_control_box, border=5, flag=wx.ALL)
        
        self.hbox2.AddSpacer(10)
        self.hbox2.Add(self.hr_xmin_control_box, border=5, flag=wx.ALL)
        self.hbox2.Add(self.hr_xmax_control_box, border=5, flag=wx.ALL)
        self.hbox2.AddSpacer(20)
        self.hbox2.Add(self.hr_ymin_control_box, border=5, flag=wx.ALL)
        self.hbox2.Add(self.hr_ymax_control_box, border=5, flag=wx.ALL)
        
        self.hbox3 = wx.BoxSizer(wx.VERTICAL)
        self.hbox3.Add(self.com_text, border=5, flag=wx.ALL)
        self.hbox3.AddSpacer(10)
        self.hbox3.Add(self.baud_text, border=5, flag=wx.ALL)
        
        self.hbox4 = wx.BoxSizer(wx.VERTICAL)
        self.hbox4.Add(self.com_choice, border=5, flag=wx.ALL)
        self.hbox4.AddSpacer(10)
        self.hbox4.Add(self.baud_choice, border=5, flag=wx.ALL)
        self.hbox4.AddSpacer(10)
        self.hbox4.Add(self.refresh_button, border=5, flag=wx.ALL)
        
        
        self.hbox2.Add(self.hbox3,border=1, flag=wx.ALL)
        self.hbox2.Add(self.hbox4,border=1, flag=wx.ALL)
        
        self.hbox5= wx.BoxSizer(wx.VERTICAL)
        self.hbox5.Add(self.vbox,border=1, flag=wx.ALL)
        self.hbox5.Add(self.hbox1,border=1, flag=wx.ALL)
        self.hbox5.Add(self.hbox2,border=1, flag=wx.ALL)
        
        #self.vbox.Add(self.hbox1, 0, flag=wx.ALIGN_LEFT | wx.TOP)
        #self.vbox.Add(self.hbox2, 0, flag=wx.ALIGN_LEFT | wx.TOP)

        self.panel.SetSizer(self.hbox5)
        self.hbox5.Fit(self)

        
    def create_graph_plots(self):
        
        self.figure ,(self.gsrAxes,self.hrAxes,self.stressAxes) = plt.subplots(1,3, sharey=False,figsize=(15,4.5),gridspec_kw = {'width_ratios':[2,1,2]})
        self.figure.tight_layout()
        plt.subplots_adjust(top=0.9)
        plt.subplots_adjust(left=0.05)

        #self.gsrAxes.set_facecolor('black')
        self.gsrAxes.set_title('SKIN CONDUCTANCE', size=12)
        self.gsrAxes.grid(color='grey')
        
        #self.hrAxes.set_facecolor('black')
        self.hrAxes.set_title('HEART RESPONSE', size=12)
        self.hrAxes.grid(color='grey')
        
        #self.barAxes.set_facecolor('black')
        self.stressAxes.set_title('STRESS LEVEL', size=12)
        self.stressAxes.grid(color='grey')
        
        plt.setp(self.gsrAxes.get_xticklabels(), fontsize=8)
        plt.setp(self.gsrAxes.get_yticklabels(), fontsize=8)

        plt.setp(self.hrAxes.get_xticklabels(), fontsize=8)
        plt.setp(self.hrAxes.get_yticklabels(), fontsize=8)
        self.hrAxes.get_yaxis().set_visible(True)
        
        plt.setp(self.stressAxes.get_xticklabels(), fontsize=8)
        plt.setp(self.stressAxes.get_yticklabels(), fontsize=8)
    
        
    def create_control_elements(self):
        
        self.gsr_xmin_control_box = BoundControlBox(self.panel, "X min", 0)
        self.gsr_xmax_control_box = BoundControlBox(self.panel, "X max", 50)
        self.gsr_ymin_control_box = BoundControlBox(self.panel, "Y min", 200)
        self.gsr_ymax_control_box = BoundControlBox(self.panel, "Y max", 1023)
        
        self.hr_xmin_control_box = BoundControlBox(self.panel, "X min", 0)
        self.hr_xmax_control_box = BoundControlBox(self.panel, "X max", 50)
        self.hr_ymin_control_box = BoundControlBox(self.panel, "Y min", 0)
        self.hr_ymax_control_box = BoundControlBox(self.panel, "Y max", 100)
        
        self.start_button = wx.Button(self.panel, -1, "Start")
        self.Bind(wx.EVT_BUTTON, self.on_start_button_click, self.start_button)
        self.Bind(wx.EVT_UPDATE_UI,self.on_start_button_update,self.start_button)
        
        self.pause_button = wx.Button(self.panel, -1, "Pause")
        self.pause_button.Disable()
        self.Bind(wx.EVT_BUTTON, self.on_pause_button_click, self.pause_button)
        self.Bind(wx.EVT_UPDATE_UI,self.on_pause_button_update,self.pause_button)
        
        
        self.save_button = wx.Button(self.panel, -1, "Save data")
        self.save_button.Disable()
        self.Bind(wx.EVT_BUTTON, self.on_save_button_click, self.save_button)
        
        self.refresh_button = wx.Button(self.panel, -1, "Refresh Ports")
        self.Bind(wx.EVT_BUTTON,self.on_refresh_button_click,self.refresh_button)        
        
        self.grid_visibility_check_box = wx.CheckBox(self.panel, -1,"Show Grid",style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX,self.on_grid_visibility_control_box_toggle,self.grid_visibility_check_box)
        self.grid_visibility_check_box.SetValue(True)

        self.xlabels_visibility_check_box = wx.CheckBox(self.panel, -1,"Show X labels",style=wx.ALIGN_RIGHT)
        #self.Bind(wx.EVT_CHECKBOX,self.on_xlabels_visibility_check_box_toggle,self.xlabels_visibility_check_box)
        self.xlabels_visibility_check_box.SetValue(True)
        
        
        self.BPM_text = wx.StaticText(self.panel,-1, label="BPM:")
        self.font = wx.Font(18, wx.DECORATIVE, wx.ITALIC, wx.BOLD)
        self.BPM_text.SetFont(self.font)
        
        self.textBpm = wx.TextCtrl(self.panel,style=wx.TE_READONLY|wx.TE_RICH|wx.NO_BORDER|wx.TRANSPARENT_WINDOW)
        #self.textBpm.SetBackgroundColour("#808080")
        self.textBpm.SetFont(self.font)
        #self.port_selection_box = wx.StaticBox(self.panel,-1, "PORT SELECTION")
        #self.port_selection_sizer = wx.BoxSizer(wx.VERTICAL)
        self.com_text = wx.StaticText(self.panel,-1, label="PORT")
        self.coms = list() 
        
        for n, (portname, desc, hwid) in enumerate(sorted(serial.tools.list_ports.comports())):
            self.coms.append(portname)  
        
        self.com_choice = wx.Choice(self.panel,choices = self.coms)
        self.com_choice.SetSelection(0)
        
        
        self.baud_text = wx.StaticText(self.panel,-1, label="BAUDRATE")
        self.baudrates = ['9600', '19200', '115200',] 
        self.baud_choice = wx.Choice(self.panel,choices = self.baudrates)
        self.baud_choice.SetSelection(2)

        #self.port_selection_sizer.Add(self.com_text, border=5, flag=wx.ALL)
        #self.port_selection_sizer.Add(self.baudrate_text, border=5, flag=wx.ALL)

        #self.SetSizer(sizer)
        #sizer.Fit(self
    def create_status_bar(self):
        self.status_bar = self.CreateStatusBar()

    
    def on_refresh_button_click(self,event):
        self.com_choice.Enable()
        self.baud_choice.Enable()
        self.scan_ports()
               
    def on_pause_button_click(self, event):
        self.paused = not self.paused
        
    def on_save_button_click(self,event):
        self.now=datetime.datetime.now()
        self.fileName= str(str(self.now.day)+"-" + str(self.now.month)+"-" +str(self.now.year)+"-" +str(self.now.hour) +"-"+ str(self.now.minute))
        self.dlg = wx.TextEntryDialog(self,'Enter file name','Text Entry')
        self.dlg.SetValue(self.fileName)
        if self.dlg.ShowModal() == wx.ID_OK:
            self.fileName=self.dlg.GetValue()
            self.dataSaver=dataLogger.dataLogger()
            self.dataSaver.saveFile(self.fileName,np.array(self.timeStamp),np.array(self.gsrData),np.array(self.HeartData),np.array(self.hrData),np.array(self.stressData))        
        self.dlg.Destroy()

        
        
    def on_start_button_click(self,event):
        self.selected_baudrate= self.baud_choice.GetString(self.baud_choice.GetSelection())
        self.selected_com_port= self.com_choice.GetString(self.com_choice.GetSelection())
        if(self.started):
            self.serial_object.__del__()
            self.flash_status_message("PLOTTING STOPPED")
            self.redraw_timer.Stop()
            
            self.gsrAxes.clear()
            self.hrAxes.clear()
            self.stressAxes.clear()
            
            
            self.gsrAxes.set_title('SKIN CONDUCTANCE', size=12)
            self.gsrAxes.grid(color='grey')
        
            #self.hrAxes.set_facecolor('black')
            self.hrAxes.set_title('HEART RESPONSE', size=12)
            #self.hrAxes.grid(color='grey')
        
            #self.barAxes.set_facecolor('black')
            self.stressAxes.set_title('STRESS LEVEL', size=12)
            #self.barAxes.grid(color='grey')
            
            self.started = not self.started
            
            self.pause_button.Disable()
            self.com_choice.Enable()
            self.baud_choice.Enable()
            self.paused = False
            self.pause_button.SetLabel("Pause")
            
        else:        
            if self.selected_baudrate=="" or self.selected_com_port=="":
                self.flash_status_message("Select port properties correctly")
            else:
                self.flash_status_message("PORT: " + self.selected_com_port + "  " + "Baudrate: " + self.selected_baudrate)
                
                #Plot the data and save the reference to the plotted line
                
                try:
                    self.serial_object= serialData.serialInit(self.selected_com_port,self.selected_baudrate)
                    
                    self.gsrData=np.array([512])
                    self.hrData=np.array([0])
                    self.sampledGSR=np.array([0])
                    self.stressData=np.array([0])
                    self.HeartData=np.array([0,0,0,0])
                    
                    self.gsrPlotData = self.gsrAxes.plot(self.gsrData, linewidth=1, color=(1, 0.25, 0),)[0]
                    self.hrPlotData = self.hrAxes.plot(self.hrData, linewidth=1, color=(1, 0.25, 0),)[0]
                    self.stressPlotData = self.stressAxes.plot(self.stressData, linewidth=1, color=(1, 0.25, 0),)[0]
                    
                    self.redraw_timer = wx.Timer(self)
                    self.Bind(wx.EVT_TIMER, self.redraw_plot, self.redraw_timer)
                    self.redraw_timer.Start(REFRESH_INTERVAL_MS)
                    
                    self.started = not self.started
                    self.flash_status_message("PLOTTING STARTED")
                    
                    self.pause_button.Enable()
                    self.save_button.Enable()
                    self.com_choice.Disable()
                    self.baud_choice.Disable()
                
                except TypeError:
                    self.flash_status_message("NO PORTS FOUND. PLEASE REFRESH PORTS AND TRY AGAIN.")
                
    def getBPM(self):
        self.timeElapsed= int(time.time()-self.START_TIME)      
        #print("####")
        #print(self.HeartData[-100:])
        
        #self.BPM=round(sum(self.HeartData[-60:])/(len(self.HeartData[-60:]*REFRESH_INTERVAL_MS))*600)
        #self.textBpm.SetValue(str(self.BPM))
        #self.beatCount=sum(self.HeartData)
        #self.HeartData=np.array([0,0,0,0])
        
        if(self.timeElapsed>=3):
            self.bpm=round((self.beatCount/self.timeElapsed)*60)
            #print("#####" ,end='')
            #print(self.bpm)
            self.beatCount=0
            self.HRAvgList[self.HRAvgCounter]=self.bpm
            #print (self.HRAvgList)
            self.HRAvgCounter=self.HRAvgCounter+1
            if(self.HRAvgCounter==len(self.HRAvgList)):
                self.HRAvgCounter=0
            self.START_TIME=time.time()
            self.BPM=round(sum(self.HRAvgList)/len(self.HRAvgList))
            self.textBpm.SetValue(str(self.BPM))
        #print (self.BPM)

        return self.BPM
        
    def on_start_button_update(self,event):
        label = "Stop" if self.started else "Start"
        self.start_button.SetLabel(label)
        
    def on_pause_button_update(self, event):
        label = "Resume" if self.paused else "Pause"
        self.pause_button.SetLabel(label)
        
    def on_exit(self, event):
        exit()
        self.Destroy()
        
    def scan_ports(self):
        self.coms=list()
        self.flash_status_message("Scanning ports")
        for n, (portname, desc, hwid) in enumerate(sorted(serial.tools.list_ports.comports())):
            self.coms.append(portname)        
        self.com_choice.Clear()
        self.com_choice.AppendItems(self.coms)
    def flash_status_message(self, message, display_time=3000):
        
        self.status_bar.SetStatusText("   "+message)
        self.message_timer = wx.Timer(self)
        
        self.Bind(
            wx.EVT_TIMER,
            self.on_flash_status_off,
            self.message_timer
        )
        self.message_timer.Start(display_time, oneShot=True)

    def on_flash_status_off(self, event):
        self.status_bar.SetStatusText('')
    def on_grid_visibility_control_box_toggle(self, event):
        self.draw_plot()
    
    
    def get_plot_xrange(self):
        """
        Return minimal and maximal values of plot -xaxis range to be displayed.

        Values of *x_min* and *x_max* by default are determined to show sliding
        window of last 50 elements of data set and they can be manually set.
        """
        if self.gsr_xmax_control_box.is_auto():
            gsrx_max = max(len(self.gsrData), 50)
        else: 
            gsrx_max=int(self.gsr_xmax_control_box.value)

        if self.gsr_xmin_control_box.is_auto():
            gsrx_min = gsrx_max - 50
        else: 
            gsrx_min=int(self.gsr_xmin_control_box.value)

        if self.hr_xmax_control_box.is_auto():
            hrx_max = max(len(self.hrData), 50)
        else: 
            hrx_max=int(self.hr_xmax_control_box.value)

        if self.hr_xmin_control_box.is_auto():
            hrx_min = hrx_max - 50
        else: 
            hrx_min=int(self.hr_xmin_control_box.value)
    
        strx_max = max(len(self.stressData), 150)
        strx_min = strx_max - 150
        
        return gsrx_max, gsrx_min ,hrx_max,hrx_min,strx_min,strx_max
    
    
    def get_plot_yrange(self):
        """
        Return minimal and maximal values of plot y-axis range to be displayed.

        Values of *y_min* and *y_max* are determined by finding minimal and
        maximal values of the data set and adding minimal necessary margin.

        """
        gsry_min = round(min(self.gsrData[-80:])) -100 if self.gsr_ymin_control_box.is_auto() \
            else int(self.gsr_ymin_control_box.value)

        gsry_max = round(max(self.gsrData[-80:])) + 100 if self.gsr_ymax_control_box.is_auto() \
            else int(self.gsr_ymax_control_box.value)

        
        hry_min = 0 if self.hr_ymin_control_box.is_auto() \
            else int(self.hr_ymin_control_box.value)

        hry_max = 250 if self.hr_ymax_control_box.is_auto() \
            else int(self.hr_ymax_control_box.value)
        
        stry_max = round(max(self.stressData))+600
        stry_min = round(min(self.stressData))+100 
        
        return gsry_min, gsry_max , hry_min, hry_max,stry_min,stry_max
    

    def redraw_plot(self,event):
        if not self.paused:
            self.gsd=self.serial_object.getGsrData()
            self.hrd=self.serial_object.getHrData()
            
            #print(self.hrData)
            #if(self.hrd and self.HeartData[-1]==0 and self.HeartData[-2]==0):
            #    self.HeartData= np.append(self.HeartData,1)
            #else:
            #    self.HeartData= np.append(self.HeartData,0)
             
            #print (self.HeartData)
            self.timeStamp.append(int(time.time()-self.PROGRAM_START_TIME))
            self.dx=0.5
                
            if (self.gsd==-1):
                self.flash_status_message("Missing frame. Waiting for the remote device to send data...",display_time=1000)
            else:
                self.gsrData=np.append(self.gsrData,self.gsd) #np.convolve(np.ones(2)/2 , 
                if(self.gsd==1023):
                    #print(self.gsrData[-1])
                    self.gsrData[-1]=self.gsrData[-2]
                    time.sleep(1)
                elif(len(self.gsrData)%10==0):
                    self.sampledGSR= np.append(self.sampledGSR,self.gsd)
                    if(len(self.sampledGSR)>6):
                        self.stressData=np.append(self.stressData,abs(self.sampledGSR[-1]-self.sampledGSR[-2])/self.dx  +  sum(self.sampledGSR[-5:])/6)
                    #print(self.stressData)
        
                if(self.hrd and self.prevHrd==False and self.HRshapeIter==0):
                    self.maxDataLength=len(self.designedHRshape);
                    self.HRshapeIter=self.maxDataLength
                    self.beatCount=self.beatCount+1
                    self.prevHrd=self.hrd
                      
                if(self.HRshapeIter!=0):
                        self.hrData=np.append(self.hrData,self.designedHRshape[self.maxDataLength-self.HRshapeIter])  
                        self.HRshapeIter=self.HRshapeIter-1    
                    
                elif(self.HRshapeIter==0 and self.hrd==False):
                    self.hrData=np.append(self.hrData,250)
                    self.prevHrd=self.hrd
                self.draw_data()
                
                    
    def draw_data(self):
        
        gsrx_min, gsrx_max ,hrx_min,hrx_max,strx_min,strx_max = self.get_plot_xrange()
        gsry_min, gsry_max ,hry_min, hry_max,stry_min,stry_max =  self.get_plot_yrange()

        self.gsrAxes.set_xbound(lower=gsrx_min, upper=gsrx_max)
        self.gsrAxes.set_ybound(lower=gsry_min, upper=gsry_max)
        self.gsrAxes.grid(self.grid_visibility_check_box.IsChecked())
             
        self.hrAxes.set_xbound(lower=hrx_min, upper=hrx_max)
        self.hrAxes.set_ybound(lower=hry_min, upper=hry_max)
        
        self.stressAxes.set_xbound(lower=strx_min, upper=strx_max)
        self.stressAxes.set_ybound(lower=stry_min, upper=stry_max)
        
   
        
        self.stressAxes.grid(self.grid_visibility_check_box.IsChecked())
        self.stressAxes.grid(self.grid_visibility_check_box.IsChecked())

        # Set x-axis labels visibility
        plt.setp(self.gsrAxes.get_xticklabels(),visible=self.xlabels_visibility_check_box.IsChecked())
        plt.setp(self.hrAxes.get_xticklabels(),visible=self.xlabels_visibility_check_box.IsChecked())
        plt.setp(self.stressAxes.get_xticklabels(),visible=self.xlabels_visibility_check_box.IsChecked())
        
        self.gsrPlotData.set_xdata(np.arange(len(self.gsrData)))
        self.gsrPlotData.set_ydata(np.array(self.gsrData))

        self.hrPlotData.set_xdata(np.arange(len(self.HeartData)))
        self.hrPlotData.set_ydata(np.array(self.HeartData))
        
        #self.stressData= self.gsrData
        
        self.stressPlotData.set_xdata(np.arange(len(self.stressData)))
        self.stressPlotData.set_ydata(np.array(self.stressData))
        
        self.HeartData=np.append(self.HeartData,self.getBPM())
        self.canvas.draw()
        
    def stressAlgorithm(self):
        pass
    def on_plot_save(self, event):
        file_choices = "PNG (*.png)|*.png"

        dlg = wx.FileDialog(
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="plot.png",
            wildcard=file_choices,
            style=wx.FD_SAVE
        )

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=DPI)
            self.flash_status_message("Saved to {}".format(path))        
        
        
        
        
        
class BoundControlBox(wx.Panel):
    """
    A static box with a couple of radio buttons and a text box. Allows to
    switch between an automatic mode and a manual mode with an associated value.
    """
    def __init__(self, parent, label, initial_value):
        wx.Panel.__init__(self, parent)

        self._value = initial_value

        box = wx.StaticBox(self, label=label)
        sizer = wx.StaticBoxSizer(box, wx.VERTICAL)

        self.auto_radio_button = wx.RadioButton(
            self, label="Automatic", style=wx.RB_GROUP
        )

        self.manual_radio_button = wx.RadioButton(
            self, label="Manual"
        )

        self.textbox = wx.TextCtrl(
            self,
            size=(35, -1),
            value=str(self.value),
            style=wx.TE_PROCESS_ENTER
        )

        self.Bind(
            wx.EVT_UPDATE_UI,
            self.on_radio_button_checked,
            self.textbox
        )

        self.Bind(
            wx.EVT_TEXT_ENTER,
            self.on_text_enter,
            self.textbox
        )

        manual_box = wx.BoxSizer(wx.HORIZONTAL)
        manual_box.Add(
            self.manual_radio_button,
            flag=wx.ALIGN_CENTER_VERTICAL
        )
        manual_box.Add(self.textbox, flag=wx.ALIGN_CENTER_VERTICAL)

        sizer.Add(self.auto_radio_button, 0, wx.ALL, 10)
        sizer.Add(manual_box, 0, wx.ALL, 10)

        self.SetSizer(sizer)
        sizer.Fit(self)

    @property
    def value(self):
        return self._value

    def on_radio_button_checked(self, event):
        self.textbox.Enable(not self.is_auto())

    def on_text_enter(self, event):
        self._value = self.textbox.GetValue()

    def is_auto(self):
        return self.auto_radio_button.GetValue()

    
"""def parse_script_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("port", help="serial port to be used")
    parser.add_argument("-b", "--baudrate", type=int, help="port baud rate")
    parser.add_argument("-t", "--timeout", type=float,help="port timeout value")

    args = parser.parse_args()
    return vars(args)
   # return {key:val for key, val in vars(args).items() if val is not None}
"""

if __name__ == "__main__":

    #kwargs = parse_script_args()
    #print (kwargs)
    app = wx.App()
    app.frame = GraphFrame()
    #app.frame.draw_data()
    app.frame.Show()
    app.MainLoop()
