import sys
from cx_Freeze import setup, Executable

setup(
    name = "On Dijkstra's Algorithm",
    version = "3.6",
    description = "A Dijkstra's Algorithm help tool.",
    executables = [Executable("mainWindow.py", base = "Win32GUI")])