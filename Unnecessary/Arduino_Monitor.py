"""
Listen to serial, return most recent numeric values
Lots of help from here:
http://stackoverflow.com/questions/1093598/pyserial-how-to-read-last-line-sent-from-serial-device
"""
from threading import Thread
import time
import serial
from xbee import XBee, ZigBee


last_received=dict()

def receiving(xbee):
    global last_received
    while True:
        last_received=xbee.wait_read_frame()
        print (last_received['samples'][0]['adc-1'])

        
class SerialData(object):
    
    def __init__(self, **kwargs):

        
        try:
            self.serial_port = serial.Serial(**kwargs)
            self.xbee=ZigBee(self.serial_port)
            print ("SERIAL PORT DETECTED")
            
        except serial.serialutil.SerialException:
            # no serial connection
            self.serial_port = None
            print ("NO SERIAL PORT DETECTED")
            exit()
        else:
            receiver=Thread(target=receiving, args=(self.xbee,))
            receiver.daemon=True
            receiver.start()

    def next(self):
        if self.serial_port is None:
            # return anything so we can test when Arduino isn't connected
            return 100
        # return a float value or try a few times until we get one
        global last_received
        for i in range(40):
            raw_line = last_received
            try:
                return raw_line['samples'][0]['adc-1']
            except ValueError:
                #print ('bogus data'),raw_line
                time.sleep(0.05)
        return 0.

    def __del__(self):
        if self.serial_port is not None:
            self.serial_port.close()

    def stopSerial(self):
        self.serial_port.close()

